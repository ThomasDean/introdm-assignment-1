import csv
import pickle
import sys
from ast import literal_eval


def csv_to_dict(file_path: str):
    reader = csv.DictReader(open(file_path, 'r', encoding='utf-8'))
    data = []
    desired_keys = ('match_id', 'lobby_type', 'radiant_win')
    n = 0
    for line in reader:
        desired_data = {key: line[key] for key in desired_keys}
        desired_data['pgroup'] = literal_eval(line['pgroup'])
        data.append(desired_data)
        n += 1
        if n > 10000:
            break
    print('done')
    filename = sys.path[0][:-3] + 'data\\cleaned_data'
    outfile = open(filename, 'wb')
    pickle.dump(data, outfile)
    outfile.close()
