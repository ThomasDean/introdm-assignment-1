import pickle
import sys


def make_hero_list():
    fp = open(str(sys.path[0][:-3]) + '\\data\\cleaned_data', 'rb')
    database = pickle.load(fp)

    tommy_list = []

    for data in database:
        try:
            record = frozenset([])
            if data['lobby_type'] != '0' and data['lobby_type'] != '7':
                pass
            elif data['radiant_win'] == 't':
                rhero1 = data['pgroup']['0']['hero_id']
                rhero2 = data['pgroup']['1']['hero_id']
                rhero3 = data['pgroup']['2']['hero_id']
                rhero4 = data['pgroup']['3']['hero_id']
                rhero5 = data['pgroup']['4']['hero_id']
                dhero1 = data['pgroup']['128']['hero_id'] * -1
                dhero2 = data['pgroup']['129']['hero_id'] * -1
                dhero3 = data['pgroup']['130']['hero_id'] * -1
                dhero4 = data['pgroup']['131']['hero_id'] * -1
                dhero5 = data['pgroup']['132']['hero_id'] * -1
                record = frozenset([rhero1, rhero2, rhero3, rhero4, rhero5, dhero1, dhero2, dhero3, dhero4, dhero5])
            else:
                rhero1 = data['pgroup']['0']['hero_id'] * -1
                rhero2 = data['pgroup']['1']['hero_id'] * -1
                rhero3 = data['pgroup']['2']['hero_id'] * -1
                rhero4 = data['pgroup']['3']['hero_id'] * -1
                rhero5 = data['pgroup']['4']['hero_id'] * -1
                dhero1 = data['pgroup']['128']['hero_id']
                dhero2 = data['pgroup']['129']['hero_id']
                dhero3 = data['pgroup']['130']['hero_id']
                dhero4 = data['pgroup']['131']['hero_id']
                dhero5 = data['pgroup']['132']['hero_id']
                record = frozenset([rhero1, rhero2, rhero3, rhero4, rhero5, dhero1, dhero2, dhero3, dhero4, dhero5])
            if len(record) == 10:
                tommy_list.append(record)
        except KeyError:
            print("Key error on record: " + str(len(tommy_list)))

    outfile = open(str(sys.path[0][:-3]) + 'data\\database', 'wb')
    print(outfile)
    pickle.dump(tommy_list, outfile)
    outfile.close()
